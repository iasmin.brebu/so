Git repo: https://gitlab.upt.ro/iasmin.brebu/so


Requirments:
Create snapshot with metadata of each file in a given directory
Up to 10 directories can be given as parameters, each will be processed by their own process, the main process only manages them
Optional directory precedeed by -o is where the snapshots will be outputed, otherwise they will be in the directory which they snapshoted


For each file found with no permissions, start a new process that: analyzes with a script, and izolates if necesarry
Izolation folder: given as parameter, preceeded by -s
Measure corruption level with sh script: #line, #words, #characters{
    #lines < 3 AND #words > 1000 AND #characters > 2000 => suspicious
    If suspicious: search for non-ASCII characters, "corrupted", "dangerous", "risk", "attack", "malware", "malicious", if any found => mark dangerous
    Dangerous => Write to stdout the file name
    Not dangerous => Write to stdout SAFE
}

Script-process communication done with pipes
