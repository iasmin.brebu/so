if [ $# -ne 1 ]; then
    exit 1
fi

if [ ! -f $1 ]; then
    exit 2
fi

lines=$(wc -l < $1)
words=$(wc -w < $1)
char=$(wc -m < $1)



if [ $lines -lt 3 ] && [ $words -gt 1000 ] && [ $char -gt 2000 ]; then
    if grep -P '[^\x00-\x7F]' "$1" >> /dev/null || grep -E 'corrupted|dangerous|risk|attack|malware|malicious' "$1" >> /dev/null; then
        echo $(basename "$1")
        exit 0
    fi
fi

echo "SAFE"
exit 0