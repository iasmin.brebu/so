/*
Requirments:
-Create snapshot with metadata of each file in a given directory
-Up to 10 directories can be given as parameters, each will be processed by their own process, the main process only manages them
-Optional directory precedeed by -o is where the snapshots will be outputed, otherwise they will be in the directory which they snapshoted

-For each file found with no permissions, start a new process that: analyze with a script, and izolate if necesarry

-Izolation folder: given as parameter, preceeded by -s
-Measure corruption level with script: #line, #words, #characters{
    #lines < 3 AND #words > 1000 AND #characters > 2000 
    => sus => search for non-ASCII characters, "corrupted", "dangerous", "risk", "attack", "malware", "malicious", if found => mark dangerous
    dangerous => stdout: name, else: stdout: safe
}

-Script-process communication done with pipes 
*/



#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <sys/wait.h>

#define MAX_DIRECTORIES 10

#define PATH_TO_ANALYZE "./analyze.sh"
#define ANALYZE_NAME "analyze.sh"

#define ERR_FD 2
#define OUT_FD 1
#define IN_FD 0

#define MAX_PATH 4096 //magic number from internet
#define MAX_NAME 256 //magic number from d_name

typedef struct{
    ino_t inode;
    mode_t mode;
    time_t modified;
    off_t size;
    char name[MAX_NAME];
}file_t;

enum{
    PARAMETER_ERROR = -1,
    ALLOC_ERROR = -2,
    CLOSING_ERROR = -3,
    CREATION_ERROR = -4
}errors_enum;

typedef struct{
    char *d[MAX_DIRECTORIES]; //directories to be snapshoted
    int nrD; //number of directories to be snapshoted
    char *s; //safe folder
    char *o; //output folder
}parameters_t;


//a tree represents a snapshot 
struct node_struct{
    file_t file;
    struct node_struct *l;
    struct node_struct *r;
};

typedef struct node_struct* tree_t;

parameters_t initializeParameters(){
    parameters_t p;
    for(int i = 0; i < 10; i++) {p.d[i] = NULL;}
    p.nrD = 0;
    p.s = NULL;
    p.o = NULL;
    return p;
}

void errorM(char *m, int code){
    write(ERR_FD, m, strlen(m));
    exit(code);
}

void warn(char *m){
    write(ERR_FD, m, strlen(m));
}

//1 = exists, 0 = does not, -1 = will be ignored
int directoryExists(char *d){
    struct stat status;
    if(stat(d, &status)){           //check if something with that name exists
        return 0;
    }
    if(S_ISDIR(status.st_mode)){    //check if dir
        return 1;
    }
    return -1;
}

int fileExists(char *d){
    struct stat status;
    if(stat(d, &status)){           //check if something with that name exists
        return 0;
    }
    if(S_ISREG(status.st_mode)){    //check if reg
        return 1;
    }
    return -1;
}

int createDirectory(char *path){
    pid_t p = fork();
    if(p < 0){
        perror("Could not create child");
        exit(CREATION_ERROR);
    }
    if(p == 0){
        if(execlp("mkdir", "mkdir", "-p", path,NULL) < 0){
            errorM("Could not open mkdir for directory creation", CREATION_ERROR);
        }
    }else{
        int state;
        if(waitpid(p, &state, 0) < 0){
            errorM("Error retrieving directory creation status", CLOSING_ERROR);
        }
        if(WIFEXITED(state)){
            return (WEXITSTATUS(state));
        }else{
            return -1;
        }
    }
    return -1; //how did ya get here?
}

parameters_t processParameters(int argc, char **argv){
    parameters_t p = initializeParameters();
    int s = 0;
    int o = 0;
    int warning = 0;
    for(int i = 1; i < argc; i++){
        if(!strcmp(argv[i], "-o")){
            if(o){
                errorM("Output folder already specified\n", PARAMETER_ERROR);
            }

            if(i >= argc - 1){
                errorM("Invalid position for -o\n", PARAMETER_ERROR);
            }
    
            if(directoryExists(argv[i+1]) == -1){
                errorM("Output folder selected is not a folder\n", PARAMETER_ERROR);
            }
            if(directoryExists(argv[i+1]) == 0){
                if(createDirectory(argv[i+1]) < 0){
                    errorM("Could not create output directory\n", CREATION_ERROR);
                }
            }
            p.o = argv[i+1];
            i++;
            o = 1;
            continue;
        }
        if(!strcmp(argv[i], "-s")){
            if(s){
                errorM("Safe folder already specified\n", PARAMETER_ERROR);
            }
            if(i >= argc - 1){
                errorM("Invalid position for -s\n", PARAMETER_ERROR);
            }
            if(directoryExists(argv[i+1]) == -1){
                errorM("Safe folder selected is not a folder\n", PARAMETER_ERROR);
            }
            if(directoryExists(argv[i+1]) == 0){
                if(createDirectory(argv[i+1]) < 0){
                    errorM("Could not create output directory\n", CREATION_ERROR);
                }
            }
            p.s = argv[i+1];
            i++;
            s = 1;
            continue;
        }
        if(directoryExists(argv[i]) == 1){
            if(p.nrD < MAX_DIRECTORIES){
                p.d[p.nrD++] = argv[i];
            }
            if(p.nrD == MAX_DIRECTORIES && !warning){
                warn("You have exceeded the limit of directories to be processed, those beyond the limit will be ignored\n");
                warning = 1;
            }
        }else{
            write(ERR_FD, argv[i], strlen(argv[i]));
            warn(" is not a valid argument and will be ignored\n");
        }
        
        
    }
    if(p.s == NULL){
        errorM("Safe folder must be specified\n", PARAMETER_ERROR);
    }
    for(int i = 0; i < p.nrD-1; i++){
        for(int j = i+1; j < p.nrD; j++){
            if(!strcmp(p.d[i], p.d[j])){
                warn("Duplicate directory has been detected and will be ignored: ");
                write(ERR_FD, p.d[i], strlen(p.d[i]));
                write(ERR_FD, "\n", 1);
                for(int k = j; k < p.nrD - 1; k++){
                    p.d[k] = p.d[k+1];
                }
                p.nrD--;
                j--;
            }
        }
    }
    return p;
}

tree_t makeNode(file_t key){
    tree_t aux = (tree_t)malloc(sizeof(struct node_struct));
    if(aux == NULL){
        perror(NULL);
        exit(ALLOC_ERROR);
    }
    aux->file = key;
    aux->l = NULL;
    aux->r = NULL;
    return aux;
}

tree_t insertFile(tree_t root, file_t key){
    if(root == NULL){
        return makeNode(key);
    }
    tree_t cur = root;
    while(1){
        if(key.inode < cur->file.inode){
            if(cur->l == NULL){
                cur->l = makeNode(key);
                return root;
            }
            cur = cur->l;
        }else{
            if(cur->r == NULL){
                cur->r = makeNode(key);
                return root;
            }
            cur = cur->r;
        }
    }
    //how did you get here?
    return NULL;
}

//also some light moving
tree_t makeTree(char *d, tree_t t, char *s, int *danger, ino_t oIno, ino_t sIno, int output){
    DIR *dStream = opendir(d);
    if(dStream == NULL){
        //fprintf(stderr, "here: %s\n", d);
        perror("Symlink likely broken");
        return t;
    }

    struct dirent *x;
    char current[MAX_PATH];
    while(1){
        errno = 0;
	char safeLocation[MAX_PATH];
        if((x = readdir(dStream)) == NULL){ //check error or reached end of stream; ignore errors;
            if(errno){              
                perror(NULL);
                continue;
            }
            break;
        }
        sprintf(current, "%s/%s", d, x->d_name);
        struct stat status;
        stat(current, &status);
        if(S_ISREG(status.st_mode) && strstr(x->d_name, "snapshot_") == NULL){
            file_t f;
            f.inode = status.st_ino;
            f.mode = status.st_mode;
            f.modified = status.st_mtime;
            strcpy(f.name,x->d_name);
            f.size = status.st_size;
            if(f.mode & (S_IRWXU | S_IRWXG | S_IRWXO)){
                t = insertFile(t, f);
            }else{
                if(chmod(current, f.mode | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH) < 0){
			perror("Could not change permissions of suspect file for analysis");
			continue;
		} 
		int pair[2];
                if(pipe(pair) < 0){
                    perror("Cannot open pipe to analyze suspicious file, file will be skipped");
                    continue;
                }
                pid_t pid = vfork();
                if(pid == 0){
                    if(close(pair[0]) < 0){
                        perror("Pipe could not be closed");
                        exit(CLOSING_ERROR);
                    }
                    if(dup2(pair[1], OUT_FD) < 0){
                        perror("Could not dupe the stdout");
                        exit(CREATION_ERROR);
                    }
                    if(execlp(PATH_TO_ANALYZE, ANALYZE_NAME, current, NULL) < 0){
                        perror("Could not open analyze tool");
                        exit(CREATION_ERROR);
                    }
                    return 0;
                }
                if(close(pair[1]) < 0){
                    perror("Pipe could not be closed, the show will go on");
                    continue;
                }
                char pipeMessage[MAX_NAME];
                char c;
                int size = 0;
                while(read(pair[0], &c, sizeof(char)) > 0){
                    //printf("%c\n",c);
                    pipeMessage[size++] = c;
                }
                pipeMessage[size-1] = '\0';
                int pidStat;
                if((waitpid(pid, &pidStat, 0)) < 0){
                    perror("Child did not return home");
                }
                
                if(WIFEXITED(pidStat) && !WEXITSTATUS(pidStat)){
                    if(strcmp(pipeMessage, "SAFE") == 0){
                        t = insertFile(t, f);
                    } 
                    
                    if(strcmp(pipeMessage, f.name) == 0){
                        
                        (*danger) = (*danger) + 1;
                        sprintf(safeLocation, "%s/%s", s, f.name);
                        
                        //printf("%s\n", safeLocation);
                        
                        if(rename(current, safeLocation) < 0){
                            perror("Could not move suspicious folder");
                        }
                    }
                }

		if(chmod(safeLocation, f.mode & ~(S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH)) < 0){
			perror("Could not change suspect file permissions after analysis");
		}		

                if(close(pair[0]) < 0){
                    perror("Pipe could not be closed, the show will go on");
                    continue;
                }
            }
            
        }
        if(S_ISDIR(status.st_mode) && strcmp(x->d_name,".") && strcmp(x->d_name, "..") && strstr(x->d_name, "snapshot_") == NULL && x->d_ino != sIno && (!oIno || x->d_ino != oIno)){
            t = makeTree(current, t, s, danger, oIno, sIno, output);
        }
    }
    if(closedir(dStream) < 0){
        perror("Error closing directory, continuing\n");
    }
    return t;
}

tree_t loadTree(int fd){
    file_t f;
    tree_t t = NULL;
    while(read(fd, &f, sizeof(file_t)) > 0){
        t = insertFile(t, f);
    }
    return t;
}

void printTreeToOUT(tree_t root){
    printf("%s (%ld)\n%ld Bytes \nLast modified: %sMode: %o\n\n",root->file.name, root->file.inode, root->file.size, asctime(gmtime(&root->file.modified)), root->file.mode);
    if(root->l != NULL){printTreeToOUT(root->l);}
    if(root->r != NULL){printTreeToOUT(root->r);}
}

void printTreeToPath(int fd, tree_t t){
    write(fd, &t->file, sizeof(file_t));
    if(t->l != NULL){printTreeToPath(fd,t->l);}
    if(t->r != NULL){printTreeToPath(fd,t->r);}
}

void freeTree(tree_t t){
    if(t == NULL){return;}
    if(t->l != NULL){freeTree(t->l);}
    if(t->r != NULL){freeTree(t->r);}
    free(t);
}

tree_t searchTree(tree_t root, file_t what){
    if(root == NULL){
        return NULL;
    }
    while(1){
        if(what.inode == root->file.inode){
            return root;
        }
        if(what.inode < root->file.inode){
            if(root->l == NULL){
                return NULL;
            }
            root = root->l;
        }
        if(what.inode > root->file.inode){
            if(root->r == NULL){
                return NULL;
            }
            root = root->r;
        }
    }

    //again, how did ya get here?
    return NULL;
}
 //asdasd
void compareTrees(tree_t c, tree_t n){
    if(c == NULL || n == NULL){
        return;
    }
    int mod = 0;
    tree_t result = searchTree(c, n->file);
    if(result != NULL){
        char message[MAX_NAME*4]; //don't ask where the size comes from
        if(strcmp(result->file.name, n->file.name)){
            sprintf(message, "%s: changed name: %s -> %s\n", result->file.name, result->file.name, n->file.name);
            write(OUT_FD, message, strlen(message));
            mod = 1;
        }
        if(result->file.size != n->file.size){
            sprintf(message, "%s: changed size: %ld -> %ld\n", result->file.name, result->file.size, n->file.size);
            write(OUT_FD, message, strlen(message));
            mod = 1;
        }
        if(result->file.modified != n->file.modified){
            sprintf(message, "%s: has been modified since last: ", result->file.name);
            strcat(message, asctime(gmtime(&c->file.modified)));
            message[strlen(message) - 1] = '\0';
            strcat(message, " -> ");
            strcat(message, asctime(gmtime(&n->file.modified)));
            write(OUT_FD, message, strlen(message));
            mod = 1;
        }
        if(result->file.mode != n->file.mode){
            sprintf(message, "%s: changed mode: %o -> %o\n", result->file.name, result->file.mode, n->file.mode);
            write(OUT_FD, message, strlen(message));
            mod = 1;
        }
        if(mod){
            write(OUT_FD, "\n", 1);
        } //////
    }
    compareTrees(c, n->l);
    compareTrees(c, n->r);
}

int processDirectory(char *d, char *o, char *s){
    int danger = 0;
    struct stat status;
    ino_t oIno;
    if(o != NULL && stat(o, &status) < 0){
        perror("Could not open output directory");
        exit(CREATION_ERROR);
    }else{
        oIno = status.st_ino;
    }
    

    if(stat(s, &status) < 0){
        perror("Could not open safe directory");
        exit(CREATION_ERROR);
    }
    ino_t sIno = status.st_ino;

    tree_t result = makeTree(d, NULL, s, &danger, oIno, sIno, o == NULL ? 1 : 0);
    tree_t existing = NULL;
    int f;
    char path[MAX_PATH];
    if(stat(d,&status) < 0){
        perror("Could not open directory");
        exit(CREATION_ERROR);
    }
    if(o == NULL){
        sprintf(path, "%s/snapshot_%ld", d, status.st_ino);
    }else{
        sprintf(path, "%s/snapshot_%ld", o, status.st_ino);
    }
    
    if(fileExists(path) == 1){
        f = open(path, O_RDONLY);
        if(f < 0){
            warn("Could not open existing snap, no comparison will be made\n");
        }else{
            existing = loadTree(f);
            //printTreeToOUT(existing);
            if(close(f)<0){
                perror("Could not close existing snapshot");
                exit(CLOSING_ERROR);
            }
        }
    }
    f = open(path, O_CREAT | O_WRONLY | O_TRUNC | O_APPEND, 0777);
    if(f < 0){
        perror("Could not create snapshot");
        exit(CREATION_ERROR);
    }
    compareTrees(existing, result);
    
    printTreeToPath(f, result);

    freeTree(existing);
    freeTree(result);
    if(close(f)<0){
        perror("Could not close snapshot");
        exit(CLOSING_ERROR);
    }
    return danger;
}

int main(int argc, char **argv)
{
    if(argc > 14 + 1){          // 4 + 10 + 1;   4 for -s and -o and their folders, 10 for what we need to save, 1 for the name
        errorM("Too many arguments, maximum: 1 output folder, 1 safe folder, 10 directories to be processed\n", PARAMETER_ERROR);
    }

    parameters_t p = processParameters(argc, argv);
    
    if(p.s == NULL){
        errorM("Safe folder must be specified\n", PARAMETER_ERROR);
    }

    char message[MAX_NAME];
    for(int i = 0; i < p.nrD; i++){
        pid_t pi = fork();
        if(pi == 0){
            int aux = processDirectory(p.d[i], p.o, p.s);
            sprintf(message, "Snapshot %s done\n", p.d[i]);
            write(OUT_FD, message, strlen(message));
            return aux;
        }
    }

    pid_t waitingPID[MAX_DIRECTORIES];
    int waitingStat[MAX_DIRECTORIES];
    for(int i = 0; i < p.nrD; i++){
        if((waitingPID[i] = wait(waitingStat+i)) < 0){
            perror("Child did not return home");
        }
    }
    for(int i = 0; i < p.nrD; i++){
       
        sprintf(message, "Child Process with pid %d terminated and found %d dangerous files\n", waitingPID[i], WEXITSTATUS(waitingStat[i]));
        write(OUT_FD, message, strlen(message));
    }
    

    return 0;
}

